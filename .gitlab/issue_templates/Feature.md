## Summary

(Summarize the proposed feature)

## Reason

(Explain why you propose this feature and why you think it is necessary)

## Detail the new feature

(Provide a technical breakdown of the proposed feature)

## Relevant info

(Paste any relevant links, images, videos or code - please use code blocks (```) to format console output, logs, and code, as it's very hard to read otherwise.)